/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import ReactNative from 'react-native';
import * as firebase from 'firebase';
const StatusBar = require('./components/StatusBar');
const ActionButton = require('./components/ActionButton');
const ListItem = require('./components/ListItem');
// Initialize Firebase
/*
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    color:'red',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
*/
const {
  AppRegistry,
  ListView,
  StyleSheet,
  Text,
  View,
  TouchableHighlight,
  AlertIOS,
} = ReactNative;
const firebaseConfig = {
  apiKey: "AIzaSyBTPdeMj74Uhs9VEHjLZnhCL_JNHj5aoa4",
  authDomain: "myroom-5341b.firebaseapp.com",
  databaseURL: "https://myroom-5341b.firebaseio.com",
  storageBucket: "gs://myroom-5341b.appspot.com",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
const styles = require('./styles.js')
 class Greeting extends Component {
  render() {
    return (
      <Text>Hello {this.props.name}!</Text>
    );
  }
}
var ds = new ListView.DataSource({ rowHasChanged: (row1, row2) => row1 !== row2 })


export default class test1 extends Component {
  constructor(props) {
  super(props);
  this.state = {
    dataSource: ds.cloneWithRows([])
  };
}
  _renderItem(item) {
    return (
      <ListItem item="{item}"  />
    );
  }
  componentDidMount() {
    this.setState({
      dataSource: ds.cloneWithRows([{ title: 'Pizza' }])
    })
  }
  render() {
    return (
      <View style="{styles.container}">

        <StatusBar title="Grocery List" />

        <ListView datasource="{this.state.dataSource}" renderrow="{this._renderItem.bind(this)}" style="{styles.listview}" />

        <ActionButton title="Add"  />

      </View>
    );
  }
}



AppRegistry.registerComponent('test1', () => test1);
