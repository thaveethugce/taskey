/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import ReactNative from 'react-native';
import * as firebase from 'firebase';
import ListItem from './components/ListItem.js';
import FBLoginView from './app/FBLoginView.js';
import ProfileScreen from './app/ProfileScreen.js';
import {
  StackNavigator,
} from 'react-navigation';
import FloatingActionButton from 'react-native-action-button';
import OneSignal from 'react-native-onesignal';
var {FBLogin, FBLoginManager} = require('react-native-facebook-login');
const StatusBar = require('./components/StatusBar');


// Initialize Firebase
/*
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    color:'red',
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
*/
const {
  AppRegistry,
  ListView,
  StyleSheet,
  Text,
  Alert,
  TextInput,
  View,
  AsyncStorage,
  ActivityIndicator,
  ToolbarAndroid,
  TouchableHighlight} = ReactNative;
const firebaseConfig = {
  apiKey: "AIzaSyBTPdeMj74Uhs9VEHjLZnhCL_JNHj5aoa4",
  authDomain: "myroom-5341b.firebaseapp.com",
  databaseURL: "https://myroom-5341b.firebaseio.com",
  storageBucket: "gs://myroom-5341b.appspot.com",
};
const firebaseApp = firebase.initializeApp(firebaseConfig);
const styles = require('./styles.js')


class test1 extends Component {
    static navigationOptions = {
         header: {
       visible: false,
     }
  };
      componentWillMount() {
        OneSignal.addEventListener('received', this.onReceived);
        OneSignal.addEventListener('opened', this.onOpened);
        OneSignal.addEventListener('registered', this.onRegistered);
        OneSignal.addEventListener('ids', this.onIds);
            AsyncStorage.getItem('userData').then((user_data_json) => {
              let userData = JSON.parse(user_data_json);
              console.log("userData ",userData);
                    this.setState({loading: false});
              if(userData){
              const { navigate } = this.props.navigation;
              navigate('Profile',{'profileData':userData});
              }
          });
    }

    componentWillUnmount() {
        OneSignal.removeEventListener('received', this.onReceived);
        OneSignal.removeEventListener('opened', this.onOpened);
        OneSignal.removeEventListener('registered', this.onRegistered);
        OneSignal.removeEventListener('ids', this.onIds);
    }

    onReceived(notification) {
        console.log("Notification received: ", notification);
    }

    onOpened(openResult) {
      console.log('Message: ', openResult.notification.payload.body);
      console.log('Data: ', openResult.notification.payload.additionalData);
      console.log('isActive: ', openResult.notification.isAppInFocus);
      console.log('openResult: ', openResult);
    }

    onRegistered(notifData) {
      console.log("test");
        console.log("Device had been registered for push notifications!", notifData);
    }

    onIds(device) {
        console.log('Device info: ', device);
    }
    OnFbLogin(data){
      const { navigate } = this.props.navigation;
      console.log("data ",data);
      AsyncStorage.setItem('userData', JSON.stringify(data));
      navigate('Profile',{'profileData':data});
    }
 constructor(props) {
    super(props);
    this.tasksRef = firebaseApp.database().ref();
    const dataSource = new ListView.DataSource({
      rowHasChanged: (row1, row2) => row1 !== row2,
    });
  this.state = {
    dataSource: dataSource,
    loading:true,
    newTask: ""
  };
  this.OnFbLogin=this.OnFbLogin.bind(this);
  }
  _addTask() {
  if (this.state.newTask === "") {
    return;
  }
  this.tasksRef.push({ name: this.state.newTask});
  Alert.alert('Alert Title','task added');
  this.setState({newTask: ""});
}
  listenForTasks(tasksRef) {
  tasksRef.on('value', (dataSnapshot) => {
    var tasks = [];
    dataSnapshot.forEach((child) => {
      tasks.push({
        name: child.val().name,
        _key: child.key
      });
    });

    this.setState({
      dataSource: this.state.dataSource.cloneWithRows(tasks)
    });
  });
}
componentDidMount() {
  // start listening for firebase updates
  this.listenForTasks(this.tasksRef);
}
  _renderItem(task) {
  return (
    <ListItem task={task} />
  );
}
  render() {
    const content = this.state.loading ? <ActivityIndicator size="large"/> : <View><View style={{height: 30}}>
      <Text style={styles.textBigHead}>Taskey</Text>
      </View>
      <View style={{height: 150}}>
        <Text>Round Robbin based task notifier</Text>
        </View>
        <View style={{height: 200}}>
        <FBLogin
    buttonView={<FBLoginView />}
    ref={(fbLogin) => { this.fbLogin = fbLogin }}
    loginBehavior={FBLoginManager.LoginBehaviors.Native}
    permissions={["email","user_friends"]}
    onLogin={this.OnFbLogin}
    onLoginFound={function(e){console.log(e)}}
    onLoginNotFound={function(e){console.log(e)}}
    onLogout={function(e){console.log(e)}}
    onCancel={function(e){console.log(e)}}
    onError={function(e){console.log(e)}}
    onPermissionsMissing={function(e){console.log(e)}}
  />
  </View></View>;
    const { navigate } = this.props.navigation;
    return (
      <View style={styles.container}>
       {content}
      </View>
    );
  }
}

const App = StackNavigator({
  Home :{screen: test1},
  Profile: {screen: ProfileScreen}
  });

AppRegistry.registerComponent('test1', () => App);
