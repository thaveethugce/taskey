var Icon = require('react-native-vector-icons/FontAwesome');
import React, { Component } from 'react';
import ReactNative from 'react-native';
var {FBLogin, FBLoginManager} = require('react-native-facebook-login');
const {
  AppRegistry,
  ListView,
  StyleSheet,
  Text,
  Alert,
  TextInput,
  View,
  ToolbarAndroid,
  TouchableHighlight} = ReactNative;
  import styles from '../styles.js';

class FBLoginView extends Component {
  static contextTypes = {
    isLoggedIn: React.PropTypes.bool,
    login: React.PropTypes.func,
    logout: React.PropTypes.func,
    props: React.PropTypes.object
    };

  constructor(props) {
      super(props);
    }

    render(){
        return (
          <View style={[]}>
            <Icon.Button onPress={() => {
                if(!this.context.isLoggedIn){
                  this.context.login()
                }
              }}
              color={"#ffffff"}
              backgroundColor={"#F13331"} borderColor={"#ff7680"} name={"facebook"}  size={20} borderRadius={3} >
            <Text style={styles.textBig}>Login with facebook!</Text>
            </Icon.Button>
          </View>
      )
    }
}
module.exports = FBLoginView;